package usernameak.modui.layout;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.WeakHashMap;
import javax.swing.JScrollPane;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.renderer.GlStateManager;
import usernameak.modui.widget.Widget;

public class LayoutWidget extends Widget {
    private List<Widget> children = new ArrayList<>();

    private Set<Widget> mouseEnteredWidgets = Collections.newSetFromMap(new WeakHashMap<>());

    @Getter
    @Setter
    private LayoutManager layout;

    public List<Widget> getChildren() {
        return Lists.newArrayList(children);
    }

    public void add(Widget child, Object layoutData) {
        add(child);
        child.setLayoutData(layoutData);
    }

    public void add(Widget child) {
        insertBefore(null, child);
    }

    public void insertBefore(Widget beforeWidget, Widget child, Object layoutData) {
        insertBefore(beforeWidget, child);
        child.setLayoutData(layoutData);
    }

    public void insertBefore(Widget beforeWidget, Widget child) {
        if (child.getParent() != null) {
            throw new IllegalStateException("child cannot have more than one parent");
        }
        if (beforeWidget != null) {
            for (int i = 0; i < children.size(); i++) {
                if (children.get(i) == beforeWidget) {
                    children.add(i, child);
                    break;
                } else if (i == children.size() - 1) {
                    throw new IllegalStateException("widget not found");
                }
            }
        } else {
            children.add(child);
        }
        child.setParent(this);
        setLayoutDirty(true);
    }

    public void replace(Widget child, Widget replacement) {
        ListIterator<Widget> li = children.listIterator();
        while (li.hasNext()) {
            Widget widget = li.next();
            if (widget == child) {
                li.set(replacement);
                child.setParent(null);
                replacement.setParent(this);
                setLayoutDirty(true);
                return;
            }
        }
        throw new IllegalStateException("child is not ancestor of me for replacement");
    }

    public void remove(Widget child) {
        if (child.getParent() != this) {
            throw new IllegalStateException("child has wrong parent");
        }
        children.remove(child);
        child.setParent(null);
        setLayoutDirty(true);
    }

    public void clear() {
        getChildren().forEach(this::remove);
    }

    @Override
    public Dimension initialMeasure() {
        return layout == null ? new Dimension() : layout.initialMeasure(this);
    }

    public boolean needsLayout() {
        if (this.isParentHadRelayout()) {
            return true;
        }
        for (Widget child : children) {
            if (child.isLayoutDirty()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isLayoutDirty() {
        return needsLayout();
    }

    @Override
    public void update() {
        super.update();

        if (layout != null && needsLayout()) {
            setParentHadRelayout(false);
            getChildren().forEach(w -> w.setParentHadRelayout(true));
            layout.layout(this);

            // getChildren().forEach(w -> System.out.println(w + ": " + w.getBounds()));
        }

        getChildren().forEach(w -> w.setLayoutDirty(false));
        getChildren().forEach(Widget::update);
    }

    @Override
    public void draw() {
        super.draw();

        getChildren().forEach(child -> {
            GlStateManager.translate(child.getPosition().x, child.getPosition().y, 0);
            child.draw();
            GlStateManager.translate(-child.getPosition().x, -child.getPosition().y, 0);
        });

        layout.drawDebug();
    }

    @Override
    public void mouseMove(int x, int y) {
        super.mouseMove(x, y);

        for (Widget child : getChildren()) {
            int cx = child.getPosition().x;
            int cy = child.getPosition().y;
            int cx1 = cx + child.getSize().width;
            int cy1 = cy + child.getSize().height;
            if (x >= cx && y >= cy && x < cx1 && y < cy1) {
                if (!mouseEnteredWidgets.contains(child)) {
                    child.mouseEnter();
                    mouseEnteredWidgets.add(child);
                }
                child.mouseMove(x - cx, y - cy);
            } else {
                if (mouseEnteredWidgets.contains(child)) {
                    child.mouseLeave();
                    mouseEnteredWidgets.remove(child);
                }
            }
        }
    }

    @Override
    public void mouseDown(int x, int y, int button) {
        super.mouseDown(x, y, button);

        for (Widget child : mouseEnteredWidgets) {
            int cx = child.getPosition().x;
            int cy = child.getPosition().y;
            child.mouseDown(x - cx, y - cy, button);
        }
    }

    @Override
    public void mouseLeave() {
        super.mouseLeave();
        for (Widget child : mouseEnteredWidgets) {
            child.mouseLeave();
        }
        mouseEnteredWidgets.clear();
    }
}
