package usernameak.modui.layout;

import lombok.Getter;
import lombok.Setter;
import usernameak.modui.mc.IModUIScreen;
import usernameak.modui.widget.Widget;

public class RootWidget extends LayoutWidget {

    public RootWidget() {
        super();
        setLayout(new RootLayout());
    }

    @Getter
    @Setter
    private IModUIScreen screen;

    @Getter
    private Widget currentTooltip;

    @Override
    public void setBounds(int x, int y, int width, int height) {
        if (getPosition().x != x || getPosition().y != y || getSize().width != width
                || getSize().height != height) {
            setLayoutDirty(true);
            setParentHadRelayout(true);
            getChildren().forEach(child -> child.setParentHadRelayout(true));
        }
        super.setBounds(x, y, width, height);
    }

    @Override
    public void update() {
        setBounds(0, 0, screen.getWidth(), screen.getHeight());
        super.update();
    }
    
    public void setCurrentTooltip(Widget tooltip) {
        if (tooltip == this.currentTooltip) {
            return;
        }
        if (this.currentTooltip != null) {
            remove(currentTooltip);
        }
        this.currentTooltip = tooltip;
        if (currentTooltip != null) {
            add(currentTooltip);
        }
        setLayoutDirty(true);
        setParentHadRelayout(true);
    }
}
