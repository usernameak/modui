package usernameak.modui.layout;

import java.awt.Dimension;

public abstract class LayoutManager {
	protected abstract Dimension initialMeasure(LayoutWidget parent);

	protected abstract void layout(LayoutWidget parent);

	protected void drawDebug() {
	}
}
