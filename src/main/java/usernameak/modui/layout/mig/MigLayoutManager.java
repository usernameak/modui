package usernameak.modui.layout.mig;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.miginfocom.layout.AC;
import net.miginfocom.layout.CC;
import net.miginfocom.layout.ComponentWrapper;
import net.miginfocom.layout.ConstraintParser;
import net.miginfocom.layout.ContainerWrapper;
import net.miginfocom.layout.Grid;
import net.miginfocom.layout.LC;
import net.miginfocom.layout.LayoutCallback;
import net.miginfocom.layout.LayoutUtil;
import net.miginfocom.layout.PlatformDefaults;
import usernameak.modui.layout.LayoutManager;
import usernameak.modui.layout.LayoutWidget;
import usernameak.modui.widget.Widget;

public class MigLayoutManager extends LayoutManager {
	/**
	 * The component to String or CC constraints mappings.
	 */
	private final Map<Widget, Object> scrConstrMap = new IdentityHashMap<Widget, Object>(8);

	private transient final Map<ComponentWrapper, CC> ccMap = new HashMap<ComponentWrapper, CC>(8);

	/**
	 * Hold the serializable text representation of the constraints.
	 */
	private Object layoutConstraints = "", colConstraints = "", rowConstraints = ""; // Should never be null!

	private transient LC lc = null;
	private transient AC colSpecs = null, rowSpecs = null;
	private transient Grid grid = null;

	private transient ArrayList<LayoutCallback> callbackList = null;

	private transient int lastModCount = PlatformDefaults.getModCount();
	private transient int lastHash = -1;

	private transient ContainerWrapper cacheParentW = null;

	/**
	 * Constructor with no constraints.
	 */
	public MigLayoutManager() {
		this("", "", "");
	}

	/**
	 * Constructor.
	 * 
	 * @param layoutConstraints The constraints that concern the whole layout.
	 *                          <code>null</code> will be treated as "".
	 */
	public MigLayoutManager(String layoutConstraints) {
		this(layoutConstraints, "", "");
	}

	/**
	 * Constructor.
	 * 
	 * @param layoutConstraints The constraints that concern the whole layout.
	 *                          <code>null</code> will be treated as "".
	 * @param colConstraints    The constraints for the columns in the grid.
	 *                          <code>null</code> will be treated as "".
	 */
	public MigLayoutManager(String layoutConstraints, String colConstraints) {
		this(layoutConstraints, colConstraints, "");
	}

	/**
	 * Constructor.
	 * 
	 * @param layoutConstraints The constraints that concern the whole layout.
	 *                          <code>null</code> will be treated as "".
	 * @param colConstraints    The constraints for the columns in the grid.
	 *                          <code>null</code> will be treated as "".
	 * @param rowConstraints    The constraints for the rows in the grid.
	 *                          <code>null</code> will be treated as "".
	 */
	public MigLayoutManager(String layoutConstraints, String colConstraints, String rowConstraints) {
		setLayoutConstraints(layoutConstraints);
		setColumnConstraints(colConstraints);
		setRowConstraints(rowConstraints);
	}

	/**
	 * Constructor.
	 * 
	 * @param layoutConstraints The constraints that concern the whole layout.
	 *                          <code>null</code> will be treated as an empty
	 *                          constraint.
	 */
	public MigLayoutManager(LC layoutConstraints) {
		this(layoutConstraints, null, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param layoutConstraints The constraints that concern the whole layout.
	 *                          <code>null</code> will be treated as an empty
	 *                          constraint.
	 * @param colConstraints    The constraints for the columns in the grid.
	 *                          <code>null</code> will be treated as an empty
	 *                          constraint.
	 */
	public MigLayoutManager(LC layoutConstraints, AC colConstraints) {
		this(layoutConstraints, colConstraints, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param layoutConstraints The constraints that concern the whole layout.
	 *                          <code>null</code> will be treated as an empty
	 *                          constraint.
	 * @param colConstraints    The constraints for the columns in the grid.
	 *                          <code>null</code> will be treated as an empty
	 *                          constraint.
	 * @param rowConstraints    The constraints for the rows in the grid.
	 *                          <code>null</code> will be treated as an empty
	 *                          constraint.
	 */
	public MigLayoutManager(LC layoutConstraints, AC colConstraints, AC rowConstraints) {
		setLayoutConstraints(layoutConstraints);
		setColumnConstraints(colConstraints);
		setRowConstraints(rowConstraints);
	}

	/**
	 * Returns layout constraints either as a <code>String</code> or
	 * {@link net.miginfocom.layout.LC} depending what was sent in to the
	 * constructor or set with {@link #setLayoutConstraints(Object)}.
	 * 
	 * @return The layout constraints either as a <code>String</code> or
	 *         {@link net.miginfocom.layout.LC} depending what was sent in to the
	 *         constructor or set with {@link #setLayoutConstraints(Object)}. Never
	 *         <code>null</code>.
	 */
	public Object getLayoutConstraints() {
		return layoutConstraints;
	}

	/**
	 * Sets the layout constraints for the layout manager instance as a String.
	 * <p>
	 * See the class JavaDocs for information on how this string is formatted.
	 * 
	 * @param s The layout constraints as a String representation. <code>null</code>
	 *          is converted to <code>""</code> for storage.
	 * @throws RuntimeException if the constraint was not valid.
	 */
	public void setLayoutConstraints(Object s) {
		if (s == null || s instanceof String) {
			s = ConstraintParser.prepare((String) s);
			lc = ConstraintParser.parseLayoutConstraint((String) s);
		} else if (s instanceof LC) {
			lc = (LC) s;
		} else {
			throw new IllegalArgumentException("Illegal constraint type: " + s.getClass().toString());
		}
		layoutConstraints = s;
		grid = null;
	}

	/**
	 * Returns the column layout constraints either as a <code>String</code> or
	 * {@link net.miginfocom.layout.AC}.
	 * 
	 * @return The column constraints either as a <code>String</code> or
	 *         {@link net.miginfocom.layout.LC} depending what was sent in to the
	 *         constructor or set with {@link #setLayoutConstraints(Object)}. Never
	 *         <code>null</code>.
	 */
	public Object getColumnConstraints() {
		return colConstraints;
	}

	/**
	 * Sets the column layout constraints for the layout manager instance as a
	 * String.
	 * <p>
	 * See the class JavaDocs for information on how this string is formatted.
	 * 
	 * @param constr The column layout constraints as a String representation.
	 *               <code>null</code> is converted to <code>""</code> for storage.
	 * @throws RuntimeException if the constraint was not valid.
	 */
	public void setColumnConstraints(Object constr) {
		if (constr == null || constr instanceof String) {
			constr = ConstraintParser.prepare((String) constr);
			colSpecs = ConstraintParser.parseColumnConstraints((String) constr);
		} else if (constr instanceof AC) {
			colSpecs = (AC) constr;
		} else {
			throw new IllegalArgumentException("Illegal constraint type: " + constr.getClass().toString());
		}
		colConstraints = constr;
		grid = null;
	}

	/**
	 * Returns the row layout constraints as a String representation. This string is
	 * the exact string as set with {@link #setRowConstraints(Object)} or sent into
	 * the constructor.
	 * <p>
	 * See the class JavaDocs for information on how this string is formatted.
	 * 
	 * @return The row layout constraints as a String representation. Never
	 *         <code>null</code>.
	 */
	public Object getRowConstraints() {
		return rowConstraints;
	}

	/**
	 * Sets the row layout constraints for the layout manager instance as a String.
	 * <p>
	 * See the class JavaDocs for information on how this string is formatted.
	 * 
	 * @param constr The row layout constraints as a String representation.
	 *               <code>null</code> is converted to <code>""</code> for storage.
	 * @throws RuntimeException if the constraint was not valid.
	 */
	public void setRowConstraints(Object constr) {
		if (constr == null || constr instanceof String) {
			constr = ConstraintParser.prepare((String) constr);
			rowSpecs = ConstraintParser.parseRowConstraints((String) constr);
		} else if (constr instanceof AC) {
			rowSpecs = (AC) constr;
		} else {
			throw new IllegalArgumentException("Illegal constraint type: " + constr.getClass().toString());
		}
		rowConstraints = constr;
		grid = null;
	}

	/**
	 * Returns a shallow copy of the constraints map.
	 * 
	 * @return A shallow copy of the constraints map. Never <code>null</code>.
	 */
	public Map<Widget, Object> getConstraintMap() {
		return new IdentityHashMap<Widget, Object>(scrConstrMap);
	}

	/**
	 * Sets the constraints map.
	 * 
	 * @param map The map. Will be copied.
	 */
	public void setConstraintMap(Map<Widget, Object> map) {
		scrConstrMap.clear();
		ccMap.clear();
		for (Map.Entry<Widget, Object> e : map.entrySet())
			setComponentConstraintsImpl(e.getKey(), e.getValue(), true);
	}

	/**
	 * Sets the component constraint for the component that already must be handled
	 * by this layout manager.
	 * <p>
	 * See the class JavaDocs for information on how this string is formatted.
	 * 
	 * @param constr  The component constraints as a String or
	 *                {@link net.miginfocom.layout.CC}. <code>null</code> is ok.
	 * @param comp    The component to set the constraints for.
	 * @param noCheck Doesn't check if control already is managed.
	 * @throws RuntimeException         if the constraint was not valid.
	 * @throws IllegalArgumentException If the component is not handling the
	 *                                  component.
	 */
	private void setComponentConstraintsImpl(Widget comp, Object constr, boolean noCheck) {
		if (noCheck == false && scrConstrMap.containsKey(comp) == false)
			throw new IllegalArgumentException("Component must already be added to parent!");

		ComponentWrapper cw = new ModUIComponentWrapper(comp);

		if (constr == null || constr instanceof String) {
			String cStr = ConstraintParser.prepare((String) constr);

			scrConstrMap.put(comp, constr);
			ccMap.put(cw, ConstraintParser.parseComponentConstraint(cStr));

		} else if (constr instanceof CC) {

			scrConstrMap.put(comp, constr);
			ccMap.put(cw, (CC) constr);

		} else {
			throw new IllegalArgumentException(
					"Constraint must be String or ComponentConstraint: " + constr.getClass().toString());
		}
		grid = null;
	}

	/**
	 * Returns if this layout manager is currently managing this component.
	 * 
	 * @param c The component to check. If <code>null</code> then <code>false</code>
	 *          will be returned.
	 * @return If this layout manager is currently managing this component.
	 */
	public boolean isManagingComponent(Widget c) {
		return scrConstrMap.containsKey(c);
	}

	/**
	 * Adds the callback function that will be called at different stages of the
	 * layout cycle.
	 * 
	 * @param callback The callback. Not <code>null</code>.
	 */
	public void addLayoutCallback(LayoutCallback callback) {
		if (callback == null)
			throw new NullPointerException();

		if (callbackList == null)
			callbackList = new ArrayList<LayoutCallback>(1);

		callbackList.add(callback);
	}

	/**
	 * Removes the callback if it exists.
	 * 
	 * @param callback The callback. May be <code>null</code>.
	 */
	public void removeLayoutCallback(LayoutCallback callback) {
		if (callbackList != null)
			callbackList.remove(callback);
	}

	/**
	 * Returns the debug millis. Combines the value from
	 * {@link net.miginfocom.layout.LC#getDebugMillis()} and
	 * {@link net.miginfocom.layout.LayoutUtil#getGlobalDebugMillis()}
	 * 
	 * @return The combined value.
	 */
	private int getDebugMillis() {
		int globalDebugMillis = LayoutUtil.getGlobalDebugMillis();
		return globalDebugMillis > 0 ? globalDebugMillis : lc.getDebugMillis();
	}

	/**
	 * Check if something has changed and if so recreate it to the cached objects.
	 * 
	 * @param parent The parent that is the target for this layout manager.
	 */
	private void checkCache(LayoutWidget parent) {
		if (parent == null)
			return;

		checkConstrMap(parent);

		ContainerWrapper par = checkParent(parent);

		// Check if the grid is valid
		int mc = PlatformDefaults.getModCount();
		if (lastModCount != mc) {
			grid = null;
			lastModCount = mc;
		}

		int hash = parent.getSize().hashCode();
		for (ComponentWrapper cw : ccMap.keySet()) {
			hash ^= cw.getLayoutHashCode();
			hash += 285134905;
		}

		if (hash != lastHash) {
			grid = null;
			lastHash = hash;
		}

		// setDebug(par, getDebugMillis() > 0);

		if (grid == null)
			grid = new Grid(par, lc, rowSpecs, colSpecs, ccMap, callbackList);
	}

	/**
	 * Checks so all components in ccMap actually exist in the parent's collection.
	 * Removes any references that don't.
	 * 
	 * @param parent The parent to compare ccMap against. Never null.
	 */
	private void checkConstrMap(LayoutWidget parent) {
		List<Widget> children = parent.getChildren();

		// Clear cached constraints
		Iterator<Map.Entry<ComponentWrapper, CC>> ccIt = ccMap.entrySet().iterator();
		while (ccIt.hasNext()) {
			Widget c = (Widget) ccIt.next().getKey().getComponent();
			if (c.isDisposed() || children.contains(c) == false) {
				ccIt.remove();
				scrConstrMap.remove(c);
				grid = null; // To clear any references to a removed widget
			}
		}

		// Recreate constraints if changed.
		for (Widget child : children) {
			Object constraint = scrConstrMap.get(child);
			if (constraint == null || !constraint.equals(child.getLayoutData()))
				setComponentConstraintsImpl(child, child.getLayoutData(), true);
		}
	}

	private ContainerWrapper checkParent(LayoutWidget parent) {
		if (parent == null)
			return null;

		if (cacheParentW == null || cacheParentW.getComponent() != parent)
			cacheParentW = new ModUIContainerWrapper(parent);

		return cacheParentW;
	}

	@Override
	protected Dimension initialMeasure(LayoutWidget parent) {
		checkCache(parent);

		int w = LayoutUtil.getSizeSafe(grid != null ? grid.getWidth() : null, LayoutUtil.PREF);
		int h = LayoutUtil.getSizeSafe(grid != null ? grid.getHeight() : null, LayoutUtil.PREF);

		return new Dimension(w, h);
	}

	@Override
	protected void layout(LayoutWidget parent) {
		checkCache(parent);

		Rectangle r = parent.getBounds();
		int[] b = new int[] { 0, 0, r.width, r.height };

		final boolean layoutAgain = grid.layout(b, lc.getAlignX(), lc.getAlignY(), getDebugMillis() > 0);

		if (layoutAgain) {
			grid = null;
			checkCache(parent);
			grid.layout(b, lc.getAlignX(), lc.getAlignY(), getDebugMillis() > 0);
		}
	}

	@Override
	protected void drawDebug() {
		super.drawDebug();
		if (grid != null)
			grid.paintDebug();
	}
}
