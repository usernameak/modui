package usernameak.modui.layout.mig;

import java.util.List;
import org.lwjgl.opengl.GL11;
import net.miginfocom.layout.ComponentWrapper;
import net.miginfocom.layout.ContainerWrapper;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import usernameak.modui.layout.LayoutWidget;
import usernameak.modui.widget.Widget;

public class ModUIContainerWrapper extends ModUIComponentWrapper implements ContainerWrapper {
    private LayoutWidget container;

    public ModUIContainerWrapper(LayoutWidget widget) {
        super(widget);
        this.container = widget;
    }

    @Override
    public ComponentWrapper[] getComponents() {
        List<Widget> cons = container.getChildren();
        ComponentWrapper[] cws = new ComponentWrapper[cons.size()];
        int i = 0;
        for (Widget widget : cons) {
            cws[i++] = new ModUIComponentWrapper(widget);
        }
        return cws;
    }

    @Override
    public void paintDebugOutline(boolean showVisualPadding) {
        GlStateManager.disableTexture2D();

        GlStateManager.glLineWidth(1.0f);

        GlStateManager.color(1, 0, 0);

        Tessellator tess = Tessellator.getInstance();
        BufferBuilder bb = tess.getBuffer();
        bb.begin(GL11.GL_LINES, DefaultVertexFormats.POSITION);
        bb.pos(0, 0, 0).endVertex();
        bb.pos(container.getSize().width, 0, 0).endVertex();

        bb.pos(container.getSize().width, 0, 0).endVertex();
        bb.pos(container.getSize().width, container.getSize().height, 0).endVertex();

        bb.pos(container.getSize().width, container.getSize().height, 0).endVertex();
        bb.pos(0, container.getSize().height, 0).endVertex();

        bb.pos(0, container.getSize().height, 0).endVertex();
        bb.pos(0, 0, 0).endVertex();
        tess.draw();

        GlStateManager.enableTexture2D();
    }

    @Override
    public int getComponentCount() {
        return container.getChildren().size();
    }

    @Override
    public Object getLayout() {
        return container.getLayout();
    }

    @Override
    public boolean isLeftToRight() {
        return true; // TODO: arabic?
    }

    @Override
    public void paintDebugCell(int x, int y, int width, int height) {

    }

    @Override
    public int getComponentType(boolean disregardScrollPane) {
        return TYPE_CONTAINER;
    }
}
