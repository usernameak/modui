package usernameak.modui.layout.mig;

import java.awt.Dimension;

import org.lwjgl.opengl.GL11;

import net.miginfocom.layout.ComponentWrapper;
import net.miginfocom.layout.ContainerWrapper;
import net.miginfocom.layout.PlatformDefaults;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import usernameak.modui.widget.Widget;

public class ModUIComponentWrapper implements ComponentWrapper {

	private Widget widget;
	private int compType = TYPE_UNKNOWN;

	public ModUIComponentWrapper(Widget widget) {
		this.widget = widget;
	}

	@Override
	public Object getComponent() {
		return widget;
	}

	@Override
	public int getX() {
		return widget.getPosition().x;
	}

	@Override
	public int getY() {
		return widget.getPosition().y;
	}

	@Override
	public int getWidth() {
		return widget.getSize().width;
	}

	@Override
	public int getHeight() {
		return widget.getSize().height;
	}

	@Override
	public int getScreenLocationX() {
		return getX();
	}

	@Override
	public int getScreenLocationY() {
		return getY();
	}

	@Override
	public int getMinimumWidth(int hHint) {
		return 0;
	}

	@Override
	public int getMinimumHeight(int wHint) {
		return 0;
	}

	@Override
	public int getPreferredWidth(int hHint) {
		return widget.initialMeasure().width;
	}

	@Override
	public int getPreferredHeight(int wHint) {
		return widget.initialMeasure().height;
	}

	@Override
	public int getMaximumWidth(int hHint) {
		return Integer.MAX_VALUE;
	}

	@Override
	public int getMaximumHeight(int wHint) {
		return Integer.MAX_VALUE;
	}

	@Override
	public void setBounds(int x, int y, int width, int height) {
		widget.setBounds(x, y, width, height);
	}

	@Override
	public boolean isVisible() {
		return widget.isVisible(); // TODO: invisibility
	}

	@Override
	public int getBaseline(int width, int height) {
		return 0;
	}

	@Override
	public boolean hasBaseline() {
		return false;
	}

	@Override
	public ContainerWrapper getParent() {
		return new ModUIContainerWrapper(widget.getParent());
	}

	@Override
	public float getPixelUnitFactor(boolean isHor) {
		return 1.0f;
	}

	@Override
	public int getHorizontalScreenDPI() {
		return PlatformDefaults.getDefaultDPI();
	}

	@Override
	public int getVerticalScreenDPI() {
		return PlatformDefaults.getDefaultDPI();
	}

	@Override
	public int getScreenWidth() {
		return 640;
	}

	@Override
	public int getScreenHeight() {
		return 480;
	}

	@Override
	public String getLinkId() {
		return null;
	}

	@Override
	public int getLayoutHashCode() {
		if (widget.isDisposed())
			return -1;

		Dimension sz = widget.getSize();
		Dimension p = widget.initialMeasure();
		int h = p.width + (p.height << 12) + (sz.width << 22) + (sz.height << 16);

		if (widget.isVisible())
			h += 1324511;

		String id = getLinkId();
		if (id != null)
			h += id.hashCode();
		return h;
	}

	@Override
	public int[] getVisualPadding() {
		return null;
	}

	@Override
	public void paintDebugOutline(boolean showVisualPadding) {
		GlStateManager.disableTexture2D();
		
		GlStateManager.glLineWidth(1.0f);

		GlStateManager.color(1, 0, 0);

		Tessellator tess = Tessellator.getInstance();
		BufferBuilder bb = tess.getBuffer();
		bb.begin(GL11.GL_LINES, DefaultVertexFormats.POSITION);
		bb.pos(widget.getPosition().x, widget.getPosition().y, 0).endVertex();
		bb.pos(widget.getPosition().x + widget.getSize().width, widget.getPosition().y, 0).endVertex();

		bb.pos(widget.getPosition().x + widget.getSize().width, widget.getPosition().y, 0).endVertex();
		bb.pos(widget.getPosition().x + widget.getSize().width, widget.getPosition().y + widget.getSize().height, 0)
				.endVertex();

		bb.pos(widget.getPosition().x + widget.getSize().width, widget.getPosition().y + widget.getSize().height, 0)
				.endVertex();
		bb.pos(widget.getPosition().x, widget.getPosition().y + widget.getSize().height, 0).endVertex();

		bb.pos(widget.getPosition().x, widget.getPosition().y + widget.getSize().height, 0).endVertex();
		bb.pos(widget.getPosition().x, widget.getPosition().y, 0).endVertex();
		tess.draw();

		GlStateManager.enableTexture2D();
	}

	@Override
	public int getComponentType(boolean disregardScrollPane) {
		return compType;
	}

	@Override
	public int getContentBias() {
		return -1;
	}

	@Override
	public final int hashCode() {
		return widget.hashCode();
	}

	@Override
	public final boolean equals(Object o) {
		if (o == null || o instanceof ComponentWrapper == false)
			return false;

		return getComponent().equals(((ComponentWrapper) o).getComponent());
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{" + widget + "}";
	}

}
