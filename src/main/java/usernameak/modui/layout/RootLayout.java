package usernameak.modui.layout;

import java.awt.Dimension;
import usernameak.modui.util.MouseHandler;
import usernameak.modui.widget.Widget;

public class RootLayout extends LayoutManager {

	@Override
	protected Dimension initialMeasure(LayoutWidget parent) {
		if(!(parent instanceof RootWidget)) {
			throw new IllegalArgumentException();
		}
		
		RootWidget rw = (RootWidget) parent;
		
		return new Dimension(rw.getScreen().getWidth(), rw.getScreen().getHeight());
	}

	@Override
	protected void layout(LayoutWidget parent) {
		Dimension dim = initialMeasure(parent);
		
		for(Widget widget : parent.getChildren()) {
			if(widget == ((RootWidget)parent).getCurrentTooltip()) {
			    Dimension wdim = widget.initialMeasure();
			    widget.setBounds(MouseHandler.getMousePosition().x, MouseHandler.getMousePosition().y, wdim.width, wdim.height);
			} else {
			    widget.setBounds(0, 0, dim.width, dim.height);
			}
		}
	}

}
