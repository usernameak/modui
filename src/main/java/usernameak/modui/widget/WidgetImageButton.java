package usernameak.modui.widget;

import java.awt.Color;

import lombok.Getter;
import lombok.Setter;
import usernameak.modui.util.UISprite;

public abstract class WidgetImageButton extends WidgetImage {
	@Setter
	@Getter
	@Deprecated
	private Color hoveredBackgroundColor = new Color(25, 25, 25, 255);
	private static final Color darkenedColor = new Color(0x80FFFFFF, true);

	public WidgetImageButton(UISprite sprite) {
		super(sprite);
		setTint(darkenedColor);
	}

	@Override
	public void mouseEnter() {
		super.mouseEnter();
		
		setTint(Color.WHITE);
	}
	
	@Override
	public void mouseLeave() {
		super.mouseLeave();
		
		setTint(darkenedColor);
	}
	
	@Override
	public void mouseDown(int x, int y, int button) {
		super.mouseDown(x, y, button);
		
		if(button == 0) {
			performAction();
		}
	}
	
	public abstract void performAction();
}
