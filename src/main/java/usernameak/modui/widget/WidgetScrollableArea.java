package usernameak.modui.widget;

import java.awt.Dimension;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.renderer.GlStateManager;

// TODO: not yet implemented
public class WidgetScrollableArea extends Widget {

    @Override
    public Dimension initialMeasure() {
        return new Dimension(1, 1);
    }
    
    @Override
    public void draw() {
        super.draw();
        
        GL11.glEnable(GL11.GL_SCISSOR_TEST);
        
        GL11.glDisable(GL11.GL_SCISSOR_TEST);
    }

}
