package usernameak.modui.widget;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import lombok.Getter;
import lombok.Setter;
import usernameak.modui.layout.LayoutWidget;
import usernameak.modui.util.RenderUtils;

public abstract class Widget {
	@Getter
	private Object layoutData = null;

	@Getter
	private Point position = new Point(0, 0);

	@Getter
	private Dimension size = new Dimension(0, 0);

	@Getter
	private LayoutWidget parent = null;

	@Getter
	@Setter
	private boolean layoutDirty = true;

	@Getter
	@Setter
	private boolean parentHadRelayout = true;

	@Getter
	@Setter
	private Color backgroundColor = new Color(0, true);
	
	@Getter
	@Setter
	private int backgroundRoundingRadius = 0;

	public boolean isDisposed() {
		return parent == null;
	}

	public boolean isVisible() {
		return true;
	}

	public abstract Dimension initialMeasure();

	public void setBounds(int x, int y, int width, int height) {
		if (position.x != x || position.y != y || size.width != width || size.height != height) {
			setLayoutDirty(true);
		}
		position.x = x;
		position.y = y;
		size.width = width;
		size.height = height;
	}

	public Rectangle getBounds() {
		return new Rectangle(position, size);
	}

	public void setLayoutData(Object layoutData) {
		if (!layoutData.equals(this.layoutData)) {
			setLayoutDirty(true);
		}
		this.layoutData = layoutData;
	}

	public void setParent(LayoutWidget parent) {
		if (parent != this.parent) {
			setLayoutDirty(true);
		}
		this.parent = parent;
	}

	public void update() {
	}

	public void draw() {
		RenderUtils.drawRectRounded(	0,
								0,
								getSize().width,
								getSize().height,
								getBackgroundColor().getRGB(), backgroundRoundingRadius);
	}
	
	public void mouseDown(int x, int y, int button) {
		mouseMove(x, y);
	}
	
	public void mouseMove(int x, int y) {
		
	}
	
	public void mouseEnter() {
		
	}
	
	public void mouseLeave() {
		
	}
}
