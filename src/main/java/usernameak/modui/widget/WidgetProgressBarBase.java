package usernameak.modui.widget;

import java.awt.Color;
import java.awt.Dimension;

import lombok.Getter;
import lombok.Setter;

public abstract class WidgetProgressBarBase extends Widget {
	@Getter
	@Setter
	private Color color = new Color(0x47CCF6);

	@Getter
	@Setter
	private float progress = 0.0f;
	
	public WidgetProgressBarBase() {
		setBackgroundColor(new Color(0x282828));
	}

	@Override
	public Dimension initialMeasure() {
		return new Dimension(1, 1);
	}


}
