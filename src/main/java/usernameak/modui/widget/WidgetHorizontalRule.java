package usernameak.modui.widget;

import java.awt.Color;
import java.awt.Dimension;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import usernameak.modui.util.RenderUtils;

@NoArgsConstructor
public class WidgetHorizontalRule extends Widget {
	@Getter
	@Setter
	private Color color = new Color(0xFFFFFFFF);
	
	public WidgetHorizontalRule(Color color) {
		super();
		this.color = color;
	}

	@Override
	public Dimension initialMeasure() {
		return new Dimension(1, 1);
	}

	@Override
	public void draw() {
		super.draw();

		RenderUtils.drawRect(	0,
								0,
								getSize().width,
								getSize().height,
								getColor().getRGB());
	}
}
