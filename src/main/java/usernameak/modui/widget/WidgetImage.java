package usernameak.modui.widget;

import java.awt.Color;
import java.awt.Dimension;

import org.lwjgl.opengl.GL11;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.GlStateManager.DestFactor;
import net.minecraft.client.renderer.GlStateManager.SourceFactor;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import usernameak.modui.util.UISprite;

public class WidgetImage extends Widget {
	@Getter
	private UISprite sprite;
	
	@Getter
	@Setter
	private Color tint = Color.WHITE;

	public WidgetImage(UISprite sprite) {
		this.sprite = sprite;
	}

	@Override
	public Dimension initialMeasure() {
		return new Dimension(sprite.getDstWidth(), sprite.getDstHeight());
	}

	public void setSprite(UISprite sprite) {
		if (sprite.getDstWidth() != this.sprite.getDstWidth() || sprite.getDstHeight() != this.sprite.getDstHeight()) {
			setLayoutDirty(true);
		}
		this.sprite = sprite;
	}

	@Override
	public void draw() {
		super.draw();

		Minecraft.getMinecraft().getTextureManager().bindTexture(sprite.getResource());

		GlStateManager.enableTexture2D();
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.ONE_MINUS_SRC_ALPHA);
		GlStateManager.shadeModel(GL11.GL_SMOOTH);
		GlStateManager.color(tint.getRed() / 255.0f, tint.getGreen() / 255.0f, tint.getBlue() / 255.0f, tint.getAlpha() / 255.0f);

		Tessellator tess = Tessellator.getInstance();
		BufferBuilder bb = tess.getBuffer();

		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		
		int cx = getSize().width / 2;
		int cy = getSize().height / 2;
		int hx = sprite.getDstWidth() / 2;
		int hy = sprite.getDstHeight() / 2;
		bb.pos(cx - hx, cy - hy, 0).tex(sprite.getMinU(), sprite.getMinV()).endVertex();
		bb.pos(cx - hx, cy + hy, 0).tex(sprite.getMinU(), sprite.getMaxV()).endVertex();
		bb.pos(cx + hx, cy + hy, 0).tex(sprite.getMaxU(), sprite.getMaxV()).endVertex();
		bb.pos(cx + hx, cy - hy, 0).tex(sprite.getMaxU(), sprite.getMinV()).endVertex();
		tess.draw();

		GlStateManager.disableBlend();
	}

}
