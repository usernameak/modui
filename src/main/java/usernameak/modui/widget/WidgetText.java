package usernameak.modui.widget;

import java.awt.Color;
import java.awt.Dimension;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.GlStateManager.DestFactor;
import net.minecraft.client.renderer.GlStateManager.SourceFactor;
import usernameak.modui.font.IFontRenderingContext;
import usernameak.modui.font.McFontRenderingContext;

public class WidgetText extends Widget {

    private static final IFontRenderingContext defaultFontRenderer =
            new McFontRenderingContext(Minecraft.getMinecraft().fontRenderer, 1.0f);

    @Getter
    private IFontRenderingContext fontRenderer = defaultFontRenderer;

    @Getter
    private String text = "";

    @Getter
    @Setter
    private Color color = Color.WHITE;

    @Getter
    private int wrapWidth = 0;

    public WidgetText(String text) {
        super();
        setText(text);
    }

    public void setFontRenderer(IFontRenderingContext fontRenderer) {
        if (!this.fontRenderer.equals(fontRenderer)) {
            setLayoutDirty(true);
        }
        this.fontRenderer = fontRenderer;
    }

    public void setText(String text) {
        if (text == null)
            throw new NullPointerException("text cannot be null");
        if (!this.text.equals(text)) {
            setLayoutDirty(true);
        }
        this.text = text;
    }

    public void setWrapWidth(int wrapWidth) {
        if (this.wrapWidth != wrapWidth) {
            setLayoutDirty(true);
        }
        this.wrapWidth = wrapWidth;
    }

    @Override
    public Dimension initialMeasure() {
        if (getWrapWidth() <= 0) {
            return new Dimension(fontRenderer.getWidth(text), getTextHeight(text));
        } else {
            return new Dimension(getWrapWidth(), getTextHeight(wrapText(text)));
        }
    }

    private int getTextHeight(String input) {
        return input.split("\\r?\\n").length * fontRenderer.getHeight();
    }

    private String wrapText(String input) {
        if (getWrapWidth() <= 0)
            return input;
        String[] spl = input.split("\\b");
        int lineWidth = 0;
        String output = "";
        for (int i = 0; i < spl.length; i++) {
            if (spl[i].contains("\n")) {
                lineWidth = 0;
                output += spl[i];
                continue;
            }
            int partWidth = fontRenderer.getWidth(spl[i]);
            if (lineWidth + partWidth >= getWrapWidth()) {
                output += "\n";
                lineWidth = 0;
            }
            output += spl[i];
            lineWidth += partWidth;
        }
        return output;
    }

    @Override
    public void draw() {
        super.draw();
        // System.out.println(getPosition());
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.ONE_MINUS_SRC_ALPHA);
        String str = wrapText(text);
        String[] lineSplit = str.split("\\r?\\n");
        int y = 0;
        for (int i = 0; i < lineSplit.length; i++) {
            fontRenderer.render(lineSplit[i], 0, y, getColor().getRGB());
            y += fontRenderer.getHeight();
        }
        GlStateManager.disableBlend();
    }

}
