package usernameak.modui.widget;

import usernameak.modui.util.RenderUtils;

public class WidgetProgressBar extends WidgetProgressBarBase {

	@Override
	public void draw() {
		super.draw();

		int progressWidth = (int) (getSize().width * getProgress());

		RenderUtils.drawRect(	0,
								0,
								progressWidth,
								getSize().height,
								getColor().getRGB());
	}
}
