package usernameak.modui.util;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class RenderUtils {

    /**
     * Draws a solid color rectangle with the specified coordinates and color.
     */
    public static void drawRect(int left, int top, int right, int bottom, int color) {
        if (left < right) {
            int i = left;
            left = right;
            right = i;
        }

        if (top < bottom) {
            int j = top;
            top = bottom;
            bottom = j;
        }

        float f3 = (float) (color >> 24 & 255) / 255.0F;
        float f = (float) (color >> 16 & 255) / 255.0F;
        float f1 = (float) (color >> 8 & 255) / 255.0F;
        float f2 = (float) (color & 255) / 255.0F;
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        GlStateManager.enableBlend();
        GlStateManager.disableTexture2D();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA,
                GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
                GlStateManager.DestFactor.ZERO);
        GlStateManager.color(f, f1, f2, f3);
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION);
        bufferbuilder.pos((double) left, (double) bottom, 0.0D).endVertex();
        bufferbuilder.pos((double) right, (double) bottom, 0.0D).endVertex();
        bufferbuilder.pos((double) right, (double) top, 0.0D).endVertex();
        bufferbuilder.pos((double) left, (double) top, 0.0D).endVertex();
        tessellator.draw();
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();
    }


    /**
     * Draws a solid color rectangle with the specified coordinates and color.
     */
    public static void drawRectRounded(int left, int top, int right, int bottom, int color,
            int radius) {
        if (right < left) {
            int i = left;
            left = right;
            right = i;
        }

        if (bottom < top) {
            int j = top;
            top = bottom;
            bottom = j;
        }

        if (radius == -1) {
            radius = Integer.min(bottom - top, right - left) / 2;
        }
        /*if (radius > 0)
            System.out.println(radius);*/

        float f3 = (float) (color >> 24 & 255) / 255.0F;
        float f = (float) (color >> 16 & 255) / 255.0F;
        float f1 = (float) (color >> 8 & 255) / 255.0F;
        float f2 = (float) (color & 255) / 255.0F;

        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        GlStateManager.enableBlend();
        GlStateManager.disableTexture2D();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA,
                GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
                GlStateManager.DestFactor.ZERO);
        GlStateManager.color(f, f1, f2, f3);
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION);
        final int PART_COUNT = 8;
        final double ANGLE_FACTOR = Math.PI / 2;
        double dx = (double) radius / PART_COUNT;
        if (radius > 0) {
            double x = left;
            for (int i = 0; i < PART_COUNT; i++) {
                double y1a = top + (1 - Math.sin((double) i / PART_COUNT * ANGLE_FACTOR)) * radius;
                double y2a = top + (1 - Math.sin((double) (i + 1) / PART_COUNT * ANGLE_FACTOR)) * radius;
                double y1b = bottom - (1 - Math.sin((double) i / PART_COUNT * ANGLE_FACTOR)) * radius;
                double y2b =
                        bottom - (1 - Math.sin((double) (i + 1) / PART_COUNT * ANGLE_FACTOR)) * radius;
                bufferbuilder.pos((double) x, (double) y1b, 0.0D).endVertex();
                bufferbuilder.pos((double) x + dx, (double) y2b, 0.0D).endVertex();
                bufferbuilder.pos((double) x + dx, (double) y2a, 0.0D).endVertex();
                bufferbuilder.pos((double) x, (double) y1a, 0.0D).endVertex();
                x += dx;
            }
        }
        bufferbuilder.pos((double) left + radius, (double) bottom, 0.0D).endVertex();
        bufferbuilder.pos((double) right - radius, (double) bottom, 0.0D).endVertex();
        bufferbuilder.pos((double) right - radius, (double) top, 0.0D).endVertex();
        bufferbuilder.pos((double) left + radius, (double) top, 0.0D).endVertex();
        if (radius > 0) {
            double x = right;
            for (int i = 0; i < PART_COUNT; i++) {
                double y1a = top + (1 - Math.sin((double) i / PART_COUNT * ANGLE_FACTOR)) * radius;
                double y2a = top + (1 - Math.sin((double) (i + 1) / PART_COUNT * ANGLE_FACTOR)) * radius;
                double y1b = bottom - (1 - Math.sin((double) i / PART_COUNT * ANGLE_FACTOR)) * radius;
                double y2b =
                        bottom - (1 - Math.sin((double) (i + 1) / PART_COUNT * ANGLE_FACTOR)) * radius;
                bufferbuilder.pos((double) x - dx, (double) y2b, 0.0D).endVertex();
                bufferbuilder.pos((double) x - dx, (double) y2a, 0.0D).endVertex();
                bufferbuilder.pos((double) x, (double) y1a, 0.0D).endVertex();
                bufferbuilder.pos((double) x, (double) y1b, 0.0D).endVertex();
                x -= dx;
            }
        }
        tessellator.draw();
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();
    }


}
