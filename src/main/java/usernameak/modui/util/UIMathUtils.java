package usernameak.modui.util;

public class UIMathUtils {
	public static final float lerp(float a, float b, float f) {
		return a + f * (b - a);
	}
}
