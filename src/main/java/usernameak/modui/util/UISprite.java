package usernameak.modui.util;

import org.lwjgl.opengl.GL11;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.util.ResourceLocation;

import static usernameak.modui.util.UIMathUtils.lerp;

@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class UISprite {
	private final ResourceLocation resource;
	private final float minU;
	private final float minV;
	private final float maxU;
	private final float maxV;
	private final int dstWidth, dstHeight;

	public UISprite(ResourceLocation resource, int dstWidth, int dstHeight) {
		super();
		this.resource = resource;
		this.dstWidth = dstWidth;
		this.dstHeight = dstHeight;
		this.minU = this.minV = 0;
		this.maxU = this.maxV = 1;
	}

	public static UISprite createFromTexture(ResourceLocation resource) {
		TextureManager textureManager = Minecraft.getMinecraft().getTextureManager();
		if (textureManager == null) {
			throw new IllegalStateException("Texture manager not initialized");
		}
		textureManager.bindTexture(resource);
		int width = GL11.glGetTexLevelParameteri(GL11.GL_TEXTURE_2D, 0, GL11.GL_TEXTURE_WIDTH);
		int height = GL11.glGetTexLevelParameteri(GL11.GL_TEXTURE_2D, 0, GL11.GL_TEXTURE_HEIGHT);
		return new UISprite(resource, width, height);
	}

	public static UISprite createTexModalRect(ResourceLocation resource, int x, int y, int width, int height) {
		float minU = x / 256.0f;
		float minV = y / 256.0f;
		float maxU = minU + (width / 256.0f);
		float maxV = minV + (width / 256.0f);
		return new UISprite(resource, minU, minV, maxU, maxV, width, height);
	}

	public static UISprite createTexModalRect(ResourceLocation resource, int x, int y, int width, int height,
			int dstWidth, int dstHeight, int atlasWidth, int atlasHeight) {
		float minU = x / (float) atlasWidth;
		float minV = y / (float) atlasHeight;
		float maxU = minU + (width / (float) atlasWidth);
		float maxV = minV + (width / (float) atlasHeight);
		return new UISprite(resource, minU, minV, maxU, maxV, dstWidth, dstHeight);
	}

	public UISprite withDstSize(int dstWidth, int dstHeight) {
		return new UISprite(resource, minU, minV, maxU, maxV, dstWidth, dstHeight);
	}

	public UISprite crop(int x, int y, int w, int h) {
		float u1 = (float) x / (float) dstWidth;
		float v1 = (float) y / (float) dstHeight;
		float u2 = (float) (x + w) / (float) dstWidth;
		float v2 = (float) (y + h) / (float) dstHeight;

		return new UISprite(resource, lerp(minU, maxU, u1), lerp(minV, maxV, v1), lerp(minU, maxU, u2),
				lerp(minV, maxV, v2), w, h);
	}
}
