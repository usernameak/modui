package usernameak.modui.mc;

import net.minecraft.client.Minecraft;
import usernameak.modui.layout.RootWidget;

public abstract class ModUIScreen {
	protected GuiModUIScreen mcScreen;
	protected OverlayGuiModUIScreen overlay;
	protected RootWidget root;
	
	public ModUIScreen() {
		root = new RootWidget();
	}
	
	public GuiModUIScreen getMcScreen() {
		if (this.mcScreen == null) {
			this.mcScreen = new GuiModUIScreen(this);
		}
		return this.mcScreen;
	}
	
	public void displayMcScreen() {
		Minecraft.getMinecraft().displayGuiScreen(getMcScreen());
	}
	
	public OverlayGuiModUIScreen createOrGetOverlay() {
		if(overlay == null) {
			overlay = new OverlayGuiModUIScreen(this);
		}
		return overlay;
	}
	
	public void update() {
		root.update();
	}
}
