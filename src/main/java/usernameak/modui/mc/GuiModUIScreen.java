package usernameak.modui.mc;

import java.awt.Point;
import java.io.IOException;
import lombok.Getter;
import net.minecraft.client.gui.GuiScreen;
import usernameak.modui.util.MouseHandler;

public class GuiModUIScreen extends GuiScreen implements IModUIScreen {
    @Getter
	private ModUIScreen modUiScreen;
	private int lastMouseX = Integer.MIN_VALUE;
	private int lastMouseY = Integer.MIN_VALUE;

	public GuiModUIScreen(ModUIScreen modUiScreen) {
		this.modUiScreen = modUiScreen;
		modUiScreen.root.setScreen(this);
		modUiScreen.root.update();
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
		MouseHandler.setMousePosition(new Point(mouseX, mouseY));
		
		if(lastMouseX != mouseX || lastMouseY != mouseY) {
			modUiScreen.root.mouseMove(mouseX, mouseY);
			lastMouseX = mouseX;
			lastMouseY = mouseY;
		}
		drawDefaultBackground();
		modUiScreen.root.draw();
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
		super.mouseClicked(mouseX, mouseY, mouseButton);
		modUiScreen.root.mouseDown(mouseX, mouseY, mouseButton);
	}
	
	@Override
	public void updateScreen() {
		super.updateScreen();
		
		modUiScreen.update();
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}
}
