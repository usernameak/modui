package usernameak.modui.mc;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;

public class OverlayGuiModUIScreen implements IModUIScreen {
	private ModUIScreen screen;

	public OverlayGuiModUIScreen(ModUIScreen screen) {
		this.screen = screen;
		screen.root.setScreen(this);
		screen.root.update();
	}

	@Override
	public int getWidth() {
		ScaledResolution scaledRes = new ScaledResolution(Minecraft.getMinecraft());
		return scaledRes.getScaledWidth();
	}

	@Override
	public int getHeight() {
		ScaledResolution scaledRes = new ScaledResolution(Minecraft.getMinecraft());
		return scaledRes.getScaledHeight();
	}

	public void render() {
		screen.root.draw();
	}

	public void update() {
		screen.update();
	}
}
