package usernameak.modui.mc;

public interface IModUIScreen {
	int getWidth();
	int getHeight();
}
