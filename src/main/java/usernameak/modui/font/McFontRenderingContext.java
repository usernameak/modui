package usernameak.modui.font;

import lombok.AllArgsConstructor;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.math.MathHelper;

@AllArgsConstructor
public class McFontRenderingContext implements IFontRenderingContext {
	private final FontRenderer fr;
	private final float scale;
	
	@Override
	public void render(String str, int x, int y, int color) {
		/*for(int i = 0; i < 255; i++) {
			ResourceLocation unicodePage = new ResourceLocation(String.format("textures/font/unicode_page_%02x.png", i));
			Minecraft.getMinecraft().getTextureManager().bindTexture(unicodePage);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		}
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("textures/font/ascii.png"));
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);*/
		
		GlStateManager.translate(x, y, 0);
		GlStateManager.scale(scale, scale, 1);
		fr.drawString(str, 0, 0, color);
		GlStateManager.scale(1.0f / scale, 1.0f / scale, 1);
		GlStateManager.translate(-x, -y, 0);
	}
	
	@Override
	public int getHeight() {
		return MathHelper.ceil(fr.FONT_HEIGHT * scale);
	}
	
	@Override
	public int getWidth(String str) {
		return MathHelper.ceil(fr.getStringWidth(str) * scale);
	}
}
