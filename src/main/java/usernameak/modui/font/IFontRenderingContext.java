package usernameak.modui.font;

public interface IFontRenderingContext {
	void render(String str, int x, int y, int color);
	int getHeight();
	int getWidth(String str);
}
